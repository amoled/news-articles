<?php

namespace App\Models\News;

use App\Models\News\Category;
use App\Models\News\Tag;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'news_articles';

    protected $fillable = ['title', 'content', 'external_url', 'featured'];

    //set default attributes
    protected $attributes = [
        'featured' => 0,
    ];

    //Define One to Many Inverse relationship between Article and Category models
    public function category()
    {
        return $this->belongsTo(Category::class, 'news_category_id', 'id');
    }

    //Define Many to Many relationship between Article and Tag models
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'news_news_tags', 'news_article_id', 'news_tag_id');
    }

    //Find Article models by title
    public function scopeTitle($query, $title)
    {
        return $query->where('title', 'LIKE', '%' . $title . '%');
    }

    //Find Article models by content
    public function scopeContent($query, $content)
    {
        return $query->where('content', 'LIKE', '%' . $content . '%');
    }
}
