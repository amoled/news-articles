<?php

namespace App\Models\News;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'news_categories';

    protected $fillable = ['title'];

    //Define One to Many relationship between Category and Article models
    public function articles()
    {
        return $this->hasMany(Article::class, 'news_category_id', 'id');
    }

    //Find Category models by title
    public function scopeTitle($query, $title)
    {
        return $query->where('title', 'LIKE', '%' . $title . '%');
    }

    //Find top three featured articles of the category
    public function featuredArticles()
    {
        return $this->hasMany(Article::class, 'news_category_id', 'id')
            ->where(function ($query) {
                $query->where('featured', 1);
            })->take(3);
    }
}
