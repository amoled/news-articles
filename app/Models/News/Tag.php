<?php

namespace App\Models\News;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'news_tags';

    protected $fillable = ['title'];

    //Define Many to Many relationship between Tag and Article models
    public function articles()
    {
        return $this->belongsToMany(Tag::class, 'news_news_tags', 'news_tag_id', 'news_article_id');
    }

    //Find Tag models by title
    public function scopeTitle($query, $title)
    {
        return $query->where('title', 'LIKE', '%' . $title . '%');
    }
}
