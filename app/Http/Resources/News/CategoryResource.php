<?php

namespace App\Http\Resources\News;

use App\Models\News\Category;
use App\Models\News\Tag;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'type' => 'news_categories',
                'id' => $this->id,
                "attributes" => [
                    'title' => $this->title,
                    'created_at' => $this->created_at,
                    'updated_at' => $this->updated_at
                ],
            ],
            'links' => [
                'self' => route('categories.show', ['category' => $this->id]),
            ],
            'included' => [
                'articles' => ArticleResource::collection($this->whenLoaded('featuredArticles')),
            ]
        ];
    }
}
