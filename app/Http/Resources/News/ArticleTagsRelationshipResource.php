<?php

namespace App\Http\Resources\News;

use App\Models\News\Article;
use Illuminate\Http\Resources\Json\JsonResource;

class ArticleTagsRelationshipResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */

    public function toArray($request)
    {
        return [
                'type' => 'news_tags',
                'id' => $this->id
        ];
    }
}
