<?php

namespace App\Http\Resources\News;

use App\Http\Resources\News\ArticleRelationshipResource;
use App\Http\Resources\News\CategoryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'type' => 'news_articles',
                'id' => $this->id,
                "attributes" => [
                    'title' => $this->title,
                    'content' => $this->content,
                    'external_url' => $this->external_url,
                    'featured' => $this->featured,
                    'created_at' => $this->created_at,
                    'updated_at' => $this->updated_at
                ],
                'relationships' => new ArticleRelationshipResource($this),
            ],
            'links' => [
                'self' => route('articles.show', ['article' => $this->id]),
            ],
            'included' => [
                'category' => new CategoryResource($this->whenLoaded('category')),
                'tags' => CategoryResource::collection($this->whenLoaded('tags')),
            ]
        ];
    }
}
