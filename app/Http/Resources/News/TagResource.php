<?php

namespace App\Http\Resources\News;

use Illuminate\Http\Resources\Json\JsonResource;

class TagResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'id' => $this->id,
                'type' => 'news_tags',
                "attributes" => [
                    'title' => $this->title,
                    'created_at' => $this->created_at,
                    'updated_at' => $this->updated_at
                ],
            ],
            'links' => [
                'self' => route('tags.show', ['tag' => $this->id]),
            ]
        ];
    }
}
