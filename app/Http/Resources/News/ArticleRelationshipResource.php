<?php

namespace App\Http\Resources\News;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleRelationshipResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $this->loadMissing(['category', 'tags']);

        return [
            'category' => [
                'links' => [
                    'self' => route('article.category.update', ['article' => $this->id]),
                    'related' => route('article.category.show', ['article' => $this->id]),
                ],
                'data' => [
                    'type' => 'news_categories',
                    'id' => $this->news_category_id
                ],
            ],
            'tags' => [
                'links' => [
                    'self' => route('article.tags.update', ['article' => $this->id]),
                    'related' => route('article.tags.show', ['article' => $this->id]),
                ],
                'data' => ArticleTagsRelationshipResource::collection($this->tags),
            ],
        ];
    }
}
