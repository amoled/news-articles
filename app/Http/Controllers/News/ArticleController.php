<?php

namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use App\Http\Requests\News\ArticleRequest;
use App\Http\Resources\News\ArticleResource;
use App\Http\Resources\News\CategoryResource;
use App\Http\Resources\News\TagResource;
use App\Models\News\Article;
use App\Models\News\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return ArticleResource
     */
    public function index(Request $request)
    {
        $articles = Article::Title($request->title)->paginate();

        if($request->include) {
            $relationships = explode(',', $request->include);
            $articles->load($relationships);
        }

        return ArticleResource::collection($articles);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $this->validatedData();

        $category = Category::find($request->news_category_id);

        if ($category instanceof Category) {
            $article = $category->articles()->create($data);
            $article->tags()->sync($request->tags);

            return (new ArticleResource($article))
                ->response()
                ->setStatusCode(Response::HTTP_CREATED);
        } else {
            return response(['error' => 'No category found with id of: ' . $request->news_category_id])
                ->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param Article $article
     * @return ArticleResource
     */
    public function show(Article $article)
    {
        return new ArticleResource($article->load('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Article $article
     * @return Response
     */
    public function edit(Article $article)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Article $article
     * @return Response
     */
    public function update(Request $request, Article $article)
    {
        $data = $this->validatedData($article->id);

        $article->update($data);

        return (new ArticleResource($article))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Article $article
     * @return Response
     */
    public function destroy(Article $article)
    {
        $article->delete();

        return response(['data' => 'Successfully deleted the news category'], Response::HTTP_NO_CONTENT);
    }

    /**
     * return the category for given article
     *
     * @param Article $article
     * @return CategoryResource
     */
    public function getCategory(Article $article)
    {
        return (new CategoryResource($article->category));
    }


    /**
     * update the category of article
     *
     * @param Request $request
     * @param Article $article
     * @return ArticleResource
     */
    public function updateCategory(Request $request, Article $article)
    {
        $category = Category::find($request->news_category_id);

        if ($category instanceof Category) {
            $article->category()->associate($category)->save();

            return (new ArticleResource($article))
                ->response()
                ->setStatusCode(Response::HTTP_OK);
        } else {
            return response(['error' => 'No category found with id of: ' . $request->news_category_id])
                ->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }


    /**
     * update the category of article
     *
     * @param Article $article
     * @return ArticleResource
     */
    public function deleteCategory(Article $article)
    {
        $article->category()->delete();

        return response(
            [
                'data' => 'Successfully deleted the article\'s category'
            ],
            Response::HTTP_NO_CONTENT);
    }


    /**
     * return the tags for given article
     *
     * @param Article $article
     * @return TagResource
     */
    public function getTags(Article $article)
    {
        return TagResource::collection($article->tags);
    }

    /**
     * update the tags of article
     *
     * @param Request $request
     * @param Article $article
     * @return TagResource
     */
    public function updateTags(Request $request, Article $article)
    {
        try {
            $article->tags()->sync($request->tags);
        }
        catch (\Illuminate\Database\QueryException $e){
            return response(
                [
                    'data' => 'The given tag(s) are not valid.',
                ],
                Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return TagResource::collection($article->tags);
    }

    /**
     * update the tags of article
     *
     * @param Article $article
     * @return ArticleResource
     */
    public function deleteTags(Article $article)
    {
        $article->tags()->delete();

        return response(
            [
                'data' => 'Successfully deleted the article\'s tags'
            ],
            Response::HTTP_NO_CONTENT);
    }

    private function validatedData($titleId = null)
    {
        return request()->validate([
            'title' => 'required|unique:news_articles,title,' . $titleId,
            'content' => 'required',
            'news_category_id' => 'sometimes|numeric',
            'external_url' => 'required|url|regex:#^(?!.*nl/).*article/(?!\d).*$#',
            'featured' => 'sometimes'
        ]);
    }
}
