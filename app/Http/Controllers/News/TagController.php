<?php

namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use App\Models\News\Tag;
use App\Http\Resources\News\TagResource;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return TagResource
     */
    public function index(Request $request)
    {
        $tags = Tag::Title($request->title)
            ->paginate();

        return TagResource::collection($tags);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $tag = Tag::create($this->validatedData());

        return (new TagResource($tag))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        return new TagResource($tag);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Tag $tag)
    {
        $data = $this->validatedData($tag->id);

        $tag->update($data);

        return (new TagResource($tag))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        $tag->delete();

        return response(['data' => 'Successfully deleted the news category'], Response::HTTP_NO_CONTENT);
    }

    private function validatedData($titleId = null)
    {
        return request()->validate([
            'title' => 'required|unique:news_tags,title,' . $titleId,
        ]);
    }
}
