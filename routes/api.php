<?php

use Illuminate\Support\Facades\Route;

Route::resource('/news/articles', 'News\ArticleController');
Route::resource('/news/categories', 'News\CategoryController');
Route::resource('/news/tags', 'News\TagController');

Route::get('/news/articles/{article}/category', 'News\ArticleController@getCategory')->name('article.category.show')->middleware('customCacheHeader');
Route::put('/news/articles/{article}/relationships/category', 'News\ArticleController@updateCategory')->name('article.category.update');
Route::delete('/news/articles/{article}/relationships/category', 'News\ArticleController@deleteCategory')->name('article.category.delete');


Route::get('/news/articles/{article}/tags', 'News\ArticleController@getTags')->name('article.tags.show');
Route::put('/news/articles/{article}/relationships/tags', 'News\ArticleController@updateTags')->name('article.tags.update');
Route::delete('/news/articles/{article}/relationships/tags', 'News\ArticleController@deleteTags')->name('article.tags.delete');


