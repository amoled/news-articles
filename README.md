# News Articles

As simple project to manage news articles, categories and tags; created with Laravel and Vue.js

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Make sure you're using the latest version of PHP.


### Installing

After cloning this project you should get the required packages, and run migrations.

run these commands in order (seeding is optional but recommended):

```
composer install
php artisan migrate
php artisan db:seed
php artisan optimize
```

Then run this command 

```
php artisan serve
```

next, open _localhost:8000_ in your browser.


## Running the tests

This project contains feature tests for Article, Category and Tag models, to run these tests:

```
php artisan config:clear
vendor\bin\phpunit
```
