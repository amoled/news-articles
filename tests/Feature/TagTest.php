<?php

namespace Tests\Feature;

use App\Models\News\Tag;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class TagTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_tag_can_be_stored()
    {
        $response = $this->post('/api/news/tags', $this->data());

        $tag = Tag::first();

        $this->assertCount(1, Tag::all());
        $this->assertEquals('Tag Title', $tag->title);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson([
            'data' => [
                'id' => $tag->id,
                'type' => 'news_tags',
                'attributes' => [
                    'title' => $tag->title
                ]
            ]
        ]);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_tag_can_be_retrieved()
    {
        $tag = factory(Tag::class)->create();

        $response = $this->get('/api/news/tags/' . $tag->id);

        $response->assertJson([
            'data' => [
                'id' => $tag->id,
                'type' => 'news_tags',
                'attributes' => [
                    'title' => $tag->title
                ]
            ]
        ]);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_tag_can_be_updated()
    {
        $tag = factory(Tag::class)->create();

        $response = $this->patch('/api/news/tags/' . $tag->id, $this->data());

        $tag = $tag->fresh();

        $this->assertCount(1, Tag::all());
        $this->assertEquals('Tag Title', $tag->title);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            'data' => [
                'id' => $tag->id,
                'type' => 'news_tags',
                'attributes' => [
                    'title' => $tag->title
                ]
            ]
        ]);
    }


    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_tag_can_be_deleted()
    {
        $tag = factory(Tag::class)->create();

        $this->assertCount(1, Tag::all());

        $response = $this->delete('/api/news/tags/' . $tag->id);

        $this->assertCount(0, Tag::all());

        $response->assertStatus(Response::HTTP_NO_CONTENT);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_list_of_tags_can_be_retrieved()
    {
        $tag = factory(Tag::class)->create();
        factory(Tag::class)->create();

        $this->assertCount(2, Tag::all());

        $response = $this->get('/api/news/tags');

        //Considering pagination, the response has three elements
        $response->assertJsonCount(3)
            ->assertJson([
                'data' => [
                    [
                        'data' => [
                            'id' => $tag->id,
                            'type' => 'news_tags',
                        ],
                    ]
                ]
            ]);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function fields_are_required()
    {
        collect(['title'])
            ->each(function ($field) {
                $response = $this->post('/api/news/tags', array_merge($this->data(), [$field => '']));

                $response->assertSessionHasErrors($field);
                $this->assertCount(0, Tag::all());
            });
    }


    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_title_must_be_unique()
    {
        $this->post('/api/news/tags', $this->data());
        $this->assertCount(1, Tag::all());

        $response = $this->post('/api/news/tags', $this->data());
        $this->assertCount(1, Tag::all());
        $response->assertSessionHasErrors();
    }


    /**
     * @return string[]
     */
    private function data(): array
    {
        return [
            'title' => 'Tag Title',
        ];
    }
}
