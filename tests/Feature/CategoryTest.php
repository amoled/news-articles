<?php

namespace Tests\Feature;

use App\Models\News\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_category_can_be_stored()
    {
        $response = $this->post('/api/news/categories', $this->data());

        $category = Category::first();

        $this->assertCount(1, Category::all());
        $this->assertEquals('Category Title', $category->title);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson([
            'data' => [
                'id' => $category->id,
                'type' => 'news_categories',
                'attributes' => [
                    'title' => $category->title
                ]
            ]
        ]);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_category_can_be_retrieved()
    {
        $category = factory(Category::class)->create();

        $response = $this->get('/api/news/categories/' . $category->id);

        $response->assertJson([
            'data' => [
                'id' => $category->id,
                'type' => 'news_categories',
                'attributes' => [
                    'title' => $category->title
                ]
            ]
        ]);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_category_can_be_updated()
    {
        $category = factory(Category::class)->create();

        $response = $this->patch('/api/news/categories/' . $category->id, $this->data());

        $category = $category->fresh();

        $this->assertCount(1, Category::all());
         $this->assertEquals('Category Title', $category->title);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            'data' => [
                'id' => $category->id,
                'type' => 'news_categories',
                'attributes' => [
                    'title' => $category->title
                ]
            ]
        ]);
    }


    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_category_can_be_deleted()
    {
        $category = factory(Category::class)->create();

        $this->assertCount(1, Category::all());

        $response = $this->delete('/api/news/categories/' . $category->id);

        $this->assertCount(0, Category::all());

        $response->assertStatus(Response::HTTP_NO_CONTENT);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_list_of_categories_can_be_retrieved()
    {
        $category = factory(Category::class)->create();
        factory(Category::class)->create();

        $this->assertCount(2, Category::all());

        $response = $this->get('/api/news/categories');

        //Considering pagination, the response has three elements
        $response->assertJsonCount(3)
            ->assertJson([
                'data' => [
                    [
                        'data' => [
                            'id' => $category->id,
                            'type' => 'news_categories'
                        ],
                    ]
                ]
            ]);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function fields_are_required()
    {
        collect(['title'])
            ->each(function ($field) {
                $response = $this->post('/api/news/categories', array_merge($this->data(), [$field => '']));

                $response->assertSessionHasErrors($field);
                $this->assertCount(0, Category::all());
            });
    }


    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_title_must_be_unique()
    {
        $this->post('/api/news/categories', $this->data());
        $this->assertCount(1, Category::all());

        $response = $this->post('/api/news/categories', $this->data());
        $this->assertCount(1, Category::all());
        $response->assertSessionHasErrors();
    }


    /**
     * @return string[]
     */
    private function data(): array
    {
        return [
            'title' => 'Category Title',
        ];
    }
}
