<?php

namespace Tests\Feature;

use App\Models\News\Category;
use App\Models\News\Article;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class RoomTest extends TestCase
{
    use RefreshDatabase;

    private Category $category;

    protected function setUp(): void
    {
        parent::setUp();

        $this->category = factory(Category::class)->create();
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function an_article_can_be_stored()
    {
        $this->withoutExceptionHandling();

        $response = $this->post('/api/news/articles', array_merge($this->data(), ['news_category_id' => $this->category->id]));

        $article = Article::first();

        $this->assertCount(1, Article::all());
        $this->assertEquals('Article Title', $article->title);
        $this->assertEquals('Article Content', $article->content);
        $this->assertEquals('http://example.com/article/why-earth-is-getting-warmer/25658', $article->external_url);
        $this->assertEquals($this->category->id, $article->news_category_id);
        $this->assertEquals($this->category->title, $article->category->title);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson([
            'data' => [
                'id' => $article->id,
                'type' => 'news_articles',
                'attributes' => [
                    'title' => $article->title,
                    'content' => $article->content,
                    'external_url' => $article->external_url,
                ]
            ]
        ]);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function an_article_can_be_retrieved()
    {
        $article = factory(Article::class)->create(['news_category_id' => $this->category->id]);

        $response = $this->get('/api/news/articles/' . $article->id);

        $response->assertJson([
            'data' => [
                'id' => $article->id,
                'type' => 'news_articles',
                'attributes' => [
                    'title' => $article->title,
                    'content' => $article->content,
                    'external_url' => $article->external_url,
                ]
            ]
        ]);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function an_article_can_be_updated()
    {
        $article = factory(Article::class)->create(['news_category_id' => $this->category->id]);

        $response = $this->patch('/api/news/articles/' . $article->id, $this->data());

        $article = $article->fresh();

        $this->assertEquals('Article Title', $article->title);
        $this->assertEquals('Article Content', $article->content);
        $this->assertEquals('http://example.com/article/why-earth-is-getting-warmer/25658', $article->external_url);
        $this->assertEquals($this->category->id, $article->news_category_id);
        $this->assertEquals($this->category->title, $article->category->title);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            'data' => [
                'id' => $article->id,
                'type' => 'news_articles',
                'attributes' => [
                    'title' => $article->title,
                    'content' => $article->content,
                    'external_url' => $article->external_url,
                ]
            ]
        ]);
    }


    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function an_article_can_be_deleted()
    {
        $article = factory(Article::class)->create(['news_category_id' => $this->category->id]);

        $this->assertCount(1, Article::all());

        $response = $this->delete('/api/news/articles/' . $article->id);

        $this->assertCount(0, Article::all());

        $response->assertStatus(Response::HTTP_NO_CONTENT);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_list_of_articles_can_be_retrieved()
    {
        $article = factory(Article::class)->create(['news_category_id' => $this->category->id]);
        factory(Article::class)->create(['news_category_id' => $this->category->id]);

        $this->assertCount(2, Article::all());

        $response = $this->get('/api/news/articles');

        //Considering pagination, the response has three elements
        $response->assertJsonCount(3)
            ->assertJson([
                'data' => [
                    [
                        'data' => [
                            'id' => $article->id,
                            'type' => 'news_articles'
                        ],
                    ]
                ]
            ]);
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function fields_are_required()
    {
        collect(['title', 'content', 'external_url'])
            ->each(function ($field) {
                $response = $this->post('/api/news/articles', array_merge($this->data(), [$field => '']));

                $response->assertSessionHasErrors($field);
                $this->assertCount(0, Article::all());
            });
    }

    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function a_title_must_be_unique()
    {
        $this->post('/api/news/articles', array_merge($this->data(), ['news_category_id' => $this->category->id]));
        $this->assertCount(1, Article::all());

        $response = $this->post('/api/news/articles', array_merge($this->data(), ['news_category_id' => $this->category->id]));
        $this->assertCount(1, Article::all());
        $response->assertSessionHasErrors();
    }


    /**
     * @test
     * A basic feature test example.
     *
     * @return void
     */
    public function an_external_url_must_conform_the_rules()
    {
        collect([
            'http://example.com/why-earth-is-getting-warmer/',
            'http://example.com/article/25658/why-earth-is-getting-warmer/',
            'http://example.com/nl/article/why-earth-is-getting-warmer/',
        ])
            ->each(function ($externalUrl) {
                $response = $this->post('/api/news/articles', array_merge($this->data(), ['external_url' => $externalUrl]));

                $response->assertSessionHasErrors('external_url');
                $response->assertSessionHasErrors();
                $this->assertCount(0, Article::all());
            });
    }

    /**
     * @return string[]
     */
    private function data(): array
    {
        return [
            'title' => 'Article Title',
            'content' => 'Article Content',
            'external_url' => 'http://example.com/article/why-earth-is-getting-warmer/25658',
        ];
    }
}
