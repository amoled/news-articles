import Vue from 'vue'
import router from './router'
import App from './components/App'
import Vuelidate from 'vuelidate'

require('./bootstrap');
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.use(Vuelidate)

const app = new Vue({
    el: '#app',
    components: {
        App
    },
    router
});
