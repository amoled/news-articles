import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeComponent from "./components/HomeComponent";
import ArticleIndex from "./views/ArticleIndex";
import ArticleCreate from "./views/ArticleCreate";
import ArticleShow from "./views/ArticleShow";
import ArticleEdit from "./views/ArticleEdit";
import CategoryIndex from "./views/CategoryIndex";
import CategoryShow from "./views/CategoryShow";
import CategoryCreate from "./views/CategoryCreate";
import CategoryEdit from "./views/CategoryEdit";
import TagIndex from "./views/TagIndex";
import TagShow from "./views/TagShow";
import TagCreate from "./views/TagCreate";
import TagEdit from "./views/TagEdit";

Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        { path: '/', component: HomeComponent},
        { path: '/articles', component: ArticleIndex},
        { path: '/articles/create', component: ArticleCreate},
        { path: '/articles/:id', component: ArticleShow},
        { path: '/articles/:id/edit', component: ArticleEdit},
        { path: '/categories', component: CategoryIndex},
        { path: '/categories/create', component: CategoryCreate},
        { path: '/categories/:id', component: CategoryShow},
        { path: '/categories/:id/edit', component: CategoryEdit},
        { path: '/tags', component: TagIndex},
        { path: '/tags/create', component: TagCreate},
        { path: '/tags/:id', component: TagShow},
        { path: '/tags/:id/edit', component: TagEdit},
    ],
    mode: 'history'
})
