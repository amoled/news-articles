<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsNewsTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_news_tags', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('news_article_id')->nullable();
            $table->unsignedBigInteger('news_tag_id')->nullable();

            $table->foreign('news_article_id')
                ->references('id')
                ->on('news_articles')
                ->onUpdate('cascade')
                ->onDelete('set null');

            $table->foreign('news_tag_id')
                ->references('id')
                ->on('news_tags')
                ->onUpdate('cascade')
                ->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_news_tags');
    }
}
