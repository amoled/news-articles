<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\News\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'title' => $faker->text(rand(5,20))
    ];
});
