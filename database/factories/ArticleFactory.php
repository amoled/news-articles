<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\News\Article;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {
    return [
        'title' => $faker->unique()->text(rand(5,20)),
        'content' => $faker->text,
        'news_category_id' => 1,
        'external_url' => $faker->url,
        'featured' => $faker->boolean
    ];
});
