<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\News\Tag;
use Faker\Generator as Faker;

$factory->define(Tag::class, function (Faker $faker) {
    return [
        'title' => $faker->text(rand(5,20))
    ];
});
