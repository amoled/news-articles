<?php

use Illuminate\Database\Seeder;

class NewsArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 1;

        while ($i <= 50) {
            $categoryID = mt_rand(1,5);

            factory(\App\Models\News\Article::class)->create(['news_category_id' => $categoryID]);

            $i++;
        }
    }
}
